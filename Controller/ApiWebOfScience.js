module.exports = function(app, axios) {
    app.post("/getSearch", function(req, res) {
      let idUser = req.body.user;
      let keywords = req.body.keywords;
      let offset = req.body.offset;
      let show = req.body.show;
      let source = req.body.source;
      console.log(source);
      let base = ["Scopus", "Web Of Science", "Google Scholar"];
  
      let body = {
        title: keywords,
        display: {
          offset: offset,
          show: show,
          sortBy: "date"
        }
      };
      console.log(source.includes("Scopus"));
      console.log(source);
  
      if (source.includes("Scopus")) {
        axios
          .put(
            "https://api.elsevier.com/content/search/sciencedirect?apiKey=5e0670b4b3e322769e657b8c5871efe8",
  
            body
          )
          .then(response => {
            response.data.source = "Scopus";
            res.send(response.data);
          })
          .catch(err => {
            console.log(err);
            let response = { data: { source: "" } };
            res.send(response.data);
          });
      } else {
        let response = { data: { source: "" } };
        response.data.source = source;
        res.send(response.data);
      }
    });
  };
  