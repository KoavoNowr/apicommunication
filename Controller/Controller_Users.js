module.exports = function(app, Model) {
	/************************************* Operateur***********************************/

	app.post('/getuser', async function(req, res) {
		Model.users
			.findAll({ where: { username: req.body.username, password: req.body.password } })
			.then((user) => {
				console.log(user);
				if (user.length > 0) {
					let response = user[0];
					res.json({ status: 1, response });
				} else {
					res.json({ status: 0 });
					res.end();
				}
			})
			.catch(function(e) {
				res.json({ status: 0 });
				console.log(e);
				res.end();
			});
	});

	app.post('/createuser', async function(req, res) {
		Model.users
			.create({
				username: 'Adam',
				password: 'adam',
				mail: 'adam@scientificsearch.com',
				university: 'um5'
			})
			.then((destroy) => {
				res.write('Created with Successfully 1');
				res.end();
			})
			.catch(function(e) {
				res.write('0');
				console.log(e);
				res.end();
			});
	});

	app.post('/deleteuser', async function(req, res) {
		var quer = 'delete from operateur where id_operateur=' + req.body.id;

		Model.Operateur
			.destroy({
				where: { id_operateur: req.body.id }
			})
			.then(() => {
				res.write('Deleted With Successfully 1');
				res.end();
			})
			.catch(function(e) {
				res.write('0');
				console.log(e);
				res.end();
			});
	});

	app.post('/listofOperateur', async function(req, res) {
		res.setHeader('Content-Type', 'application/json');

		Model.Operateur
			.findAll()
			.then((operateur) => {
				res.write(JSON.stringify(operateur, null, 1));
				res.end();
			})
			.catch(function(e) {
				//gestion erreur
				console.log(e);
			});
	});

	app.post('/update_Operateur', async function(req, res) {
		var quer =
			"update operateur set name_operateur='" +
			req.body.name +
			"',mat_operateur='" +
			req.body.mat +
			"' where id_operateur=" +
			req.body.id;

		Model.Operateur
			.update(
				{ name_operateur: req.body.name, mat_operateur: req.body.mat },
				{ where: { id_operateur: req.body.id } }
			)
			.then((user) => {
				res.write('');

				res.end();
			})
			.catch(function(e) {
				res.write('');
				res.end();

				console.log(e);
			});
	});

	/**************************************************************************/
};
