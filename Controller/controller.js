module.exports = function(app, axios, Model) {
  require("./Controller_Users")(app, Model);
  require("./Api")(app, axios);
  // require("./ApiWebOfScience")(app, axios);
};
