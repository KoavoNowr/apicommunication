module.exports = function(Sequelize, sequelize) {
  const historique = sequelize.define(
    "historique",
    {
      id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
      key: { type: Sequelize.STRING(20), allowNull: false },
      sources: { type: Sequelize.STRING(20), allowNull: false }
    },
    { tableName: "historique", timestamps: true, underscored: true }
  );

  return historique;
};
