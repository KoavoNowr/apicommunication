const uuid = require('uuid/v4');
module.exports = function(Sequelize, sequelize) {
  const users = sequelize.define(
    "users",
    {
      id_users: {
        type: Sequelize.UUID,
        defaultValue: uuid(),
        primaryKey: true
      },
      username: { type: Sequelize.STRING(30), allowNull: false },
      password: { type: Sequelize.STRING(30), allowNull: false },
      mail: { type: Sequelize.STRING(50), allowNull: false },
      university: { type: Sequelize.STRING(50), allowNull: false }
    },
    { tableName: "users", timestamps: true, underscored: true }
  );

  return users;
};
