const Sequelize = require("sequelize");
//const sequelize = new Sequelize('postgres://postgres:zakaria123@localhost:5432/test');
const sequelize = new Sequelize("scientificsearch", "postgres", "123@123@123", {
  host: "localhost",
  port: 5433,
  dialect: "postgres",
  dialectOptions: {
    useUTC: false // for reading from database
  }
  //timezone: '+1:00', // for writing to database
});
 
var exports = (module.exports = {});
exports.sequelize = sequelize;

/**
 * Operateur
 */

exports.users = require("./Users")(Sequelize, sequelize);
exports.Historique = require("./Historique")(Sequelize, sequelize);

exports.users.hasMany(exports.Historique, {
  foreignKey: { allowNull: false },
  onDelete: "CASCADE"
}); // chaque Authentification apartient a un Poste

sequelize.sync({ logging: console.log });
