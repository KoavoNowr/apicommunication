var express = require("express");
var bodyParser = require("body-parser");
var cors = require("cors");
var app = express();

app.use(cors());

app.use(bodyParser.json());

app.use(
  bodyParser.urlencoded({
    extended: false
  })
);

app.use(express.static(__dirname + "/build"));

app.get("*", function(req, res) {
  // save html files in the `views` folder...
  res.sendfile(__dirname + "/build/index.html");
});


app.listen(8765);

module.exports = app;
