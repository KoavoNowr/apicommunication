var { pg, Client } = require("pg");

const connectionString =
  "postgres://postgres:123@123@123@localhost:5433/postgres";

const client = new Client({
  connectionString: connectionString
});
client.connect();


/*client.query("CREATE DATABASE scientificsearch ", (err, res) => {
   if (err) {
       console.log(err);

   } else {
        console.log("done");
   }

});
*/

var app = require("./Tools/App.js");
var axios = require("./Tools/axios");

var Model = require("./Models/model.js");

require("./Controller/controller.js")(app, axios, Model);
